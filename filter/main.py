import os
import cv2 as cv
import numpy as np
from filters_helper import filters
from new_filters_helper import new_filters

video_path = os.path.normpath(os.getcwd() + os.sep + "videos") + os.sep + 'ESAS TİP 40X Kayıt 4.mp4'
cap = cv.VideoCapture(video_path)


fourcc = cv.VideoWriter_fourcc(*'MPEG')
out = cv.VideoWriter('output.avi', fourcc, 40, (1280, 1024))




while True:
    ret, frame = cap.read()
    if not ret:
        break

    # resize_frame = cv.resize(frame, None, fx=0.7, fy=0.7)

    lab = cv.cvtColor(frame, cv.COLOR_BGR2LAB)
    l, a, b = cv.split(lab)
    ret = new_filters.full_filter(l)

    cv.imshow("filtered output", ret)
    out.write(ret)

    # quit on ESC button
    if cv.waitKey(1) & 0xFF == 27:  # Esc pressed
        break

cv.destroyAllWindows()
out.release()