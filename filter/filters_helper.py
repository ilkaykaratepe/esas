import os
import cv2 as cv
import numpy as np
from scipy.signal.signaltools import wiener
from numpy.fft import fft2, ifft2
from skimage import img_as_ubyte
from skimage.filters import frangi


class filters:

    def __init__(self):
        pass

    def rgb_contrast_enhancement(gray_frame):
        # -----Applying CLAHE to L-channel-------------------------------------------
        clahe = cv.createCLAHE(clipLimit=0.2, tileGridSize=(3, 3))
        cl = clahe.apply(gray_frame)

        return cl

    def sharpening(gray_frame):
        l_kernel = np.array([[1, 1, 1], [1, -8, 1], [1, 1, 1]])

        # gray_frame = cv.Sobel(gray_frame, cv.CV_8U, 1, 0, ksize=1) #x
        # gray_frame = cv.Sobel(gray_frame, cv.CV_8U, 0, 1, ksize=1) #y
        return cv.filter2D(gray_frame, -1, l_kernel)

    def wiener_filter(gray_frame):
        np.seterr(divide='ignore', invalid='ignore')
        wiener_frame = wiener(gray_frame, (3, 3))
        # frangi_result = frangi(wiener_frame)
        return cv.normalize(wiener_frame, None, alpha=0, beta=255, norm_type=cv.NORM_MINMAX, dtype=cv.CV_8U)

    def gray_to_binary(gray_frame):
        return cv.bitwise_not(cv.threshold(gray_frame, 0, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)[1])

    def morphology(binary_frame, scale=40):
        # ret, binary_frame = cv.threshold(gray_frame, 0, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
        if scale == 40:
            binary_frame = cv.medianBlur(binary_frame, 5)

            kernel = cv.getStructuringElement(cv.MORPH_RECT, (3, 3))
            binary_frame = cv.morphologyEx(binary_frame, cv.MORPH_CLOSE, kernel)
            binary_frame = cv.morphologyEx(binary_frame, cv.MORPH_OPEN, kernel)
            kernel = cv.getStructuringElement(cv.MORPH_RECT, (5, 5))
            binary_frame = cv.morphologyEx(binary_frame, cv.MORPH_OPEN, kernel)

        elif scale == 20:
            binary_frame = cv.medianBlur(binary_frame, 3)

            kernel = cv.getStructuringElement(cv.MORPH_RECT, (1, 1))
            binary_frame = cv.morphologyEx(binary_frame, cv.MORPH_CLOSE, kernel)
            binary_frame = cv.morphologyEx(binary_frame, cv.MORPH_OPEN, kernel)

        return binary_frame

    def small_object_delete(binary_frame, size=30):
        colored_frame = cv.cvtColor(binary_frame, cv.COLOR_GRAY2BGR)
        # ret_img.fill(255)
        contours, hierarchy = cv.findContours(binary_frame, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        # sperm_boxes = []
        # noise_boxes = []
        for contour in contours:
            # x, y, w, h = cv.boundingRect(contour)      #
            # box = (x, y, w, h)                         # dizi döndürülecekse
            # box = np.int0(box)                         #
            if size < np.size(contour):
                # sperm_boxes.append(box)
                # cv.polylines(colored_frame, contour, True, (50, 50, 255), -1)
                cv.drawContours(colored_frame, [contour], contourIdx=0, color=(90, 90, 255), thickness=1)

            else:
                # noise_boxes.append(box)
                cv.fillConvexPoly(colored_frame, contour, (255, 255, 255))
                # cv.drawContours(binary_frame, contour, contourIdx=-1, color=(255, 255, 255), thickness=cv.FILLED)

        cv.rectangle(colored_frame, (0, 0), (colored_frame.shape[1], colored_frame.shape[0]), (255, 255, 255),
                     1)  # frame arround

        return None, None, colored_frame
    def full_filter(gray_image):
        gray_image = cv.GaussianBlur(gray_image, (3, 3), 0)
        # gray_image = filters.wiener_filter(gray_image, wiener_kernel_size)
        gray_image = filters.rgb_contrast_enhancement(gray_image)
        gray_image = filters.sharpening(gray_image)
        # gray_image = filters.rgb_contrast_enhancement(gray_image)

        kernelx = np.array(
            [[0, 0, 1, 0, 0], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1], [0, 0, 1, 0, 0]])

        gray_image = cv.filter2D(gray_image, -1, kernelx)

        binary_image = filters.gray_to_binary(gray_image)

        binary_image = filters.small_object_delete(binary_image, size=120)[2]  # 40x icin 90
        # binary_image = filters.morphology(binary_image, scale=scale)

        return binary_image
