import os
import cv2 as cv
import numpy as np
from filters_helper import filters
import time
import subprocess as sp
import multiprocessing as mp
from os import remove

file_name = os.path.normpath(os.getcwd() + os.sep + "videos") + os.sep + 'video_sperm.mp4'
output_file_name = "videos/output.avi"
cap = cv.VideoCapture(file_name)

'''cap.set(5, 60)
cap.set(cv.CAP_PROP_FRAME_WIDTH, int(0.6 * cap.get(cv.CAP_PROP_FRAME_WIDTH)))
cap.set(cv.CAP_PROP_FRAME_HEIGHT, int(0.6 * cap.get(cv.CAP_PROP_FRAME_HEIGHT)))'''

frame_count = cap.get(cv.CAP_PROP_FRAME_COUNT)
num_processes = mp.cpu_count()
frame_jump_unit = frame_count / num_processes

print("Video frame count = {}".format(frame_count))
print("Number of CPU: " + str(num_processes))


def process_video_multiprocessing(group_number):
    # Read video file
    cap.set(cv.CAP_PROP_POS_FRAMES, frame_jump_unit * group_number)

    # get height, width and frame count of the video
    width, height = (
        int(cap.get(cv.CAP_PROP_FRAME_WIDTH)),
        int(cap.get(cv.CAP_PROP_FRAME_HEIGHT))
    )
    no_of_frames = int(cap.get(cv.CAP_PROP_FRAME_COUNT))
    fps = int(cap.get(cv.CAP_PROP_FPS))
    proc_frames = 0

    # Define the codec and create VideoWriter object
    fourcc = cv.VideoWriter_fourcc(*'MPEG')
    out = cv.VideoWriter()
    out.open("output_{}.avi".format(group_number), fourcc, fps, (width, height), False)
    try:
        while proc_frames < frame_jump_unit:
            ret, frame = cap.read()
            if not ret:
                break

            lab = cv.cvtColor(frame, cv.COLOR_BGR2LAB)
            l, a, b = cv.split(lab)
            cn_frame = filters.full_filter(l)



            # write the frame
            out.write(cn_frame)

            proc_frames += 1
    except:
        # Release resources
        cap.release()
        out.release()

    # Release resources
    cap.release()
    out.release()


def combine_output_files(num_processes):
    # Create a list of output files and store the file names in a txt file
    list_of_output_files = ["output_{}.avi".format(i) for i in range(num_processes)]
    with open("list_of_output_files.txt", "w") as f:
        for t in list_of_output_files:
            f.write("file {} \n".format(t))

    # use ffmpeg to combine the video output files
    ffmpeg_cmd = "ffmpeg -y -loglevel error -f concat -safe 0 -i list_of_output_files.txt -vcodec copy " + output_file_name
    sp.Popen(ffmpeg_cmd, shell=True).wait()

    # Remove the temperory output files
    for f in list_of_output_files:
        remove(f)
    remove("list_of_output_files.txt")


def multi_process():
    print("Video processing using {} processes...".format(num_processes))
    start_time = time.time()

    # Paralle the execution of a function across multiple input values
    p = mp.Pool(num_processes)
    p.map(process_video_multiprocessing, range(num_processes))

    combine_output_files(num_processes)

    end_time = time.time()

    total_processing_time = end_time - start_time
    print("Time taken: {}".format(total_processing_time))
    print("FPS : {}".format(frame_count / total_processing_time))


if __name__ == '__main__':
    multi_process()
