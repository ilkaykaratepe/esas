import os
import cv2
import numpy as np
import sys
import math
import time
import random
from sort import *
import pandas as pd
import matplotlib.pyplot as plt

plt.style.use('seaborn-whitegrid')
np.set_printoptions(threshold=sys.maxsize)

FRAME_TIME = 0.025  ## saniye
REAL_PIXEL_MEASUREMENT_IN_40X = 0.2734375  ## mikron
REAL_PIXEL_MEASUREMENT_IN_20X = 0.545875

## video dosya yolu
video_path = os.path.normpath(os.getcwd() + os.sep + "videos") + os.sep + 'ESAS TİP 40X Kayıt 4.mp4'
cap = cv2.VideoCapture(video_path)

## çıktı videosu
fourcc = cv2.VideoWriter_fourcc(*'MPEG')
out = cv2.VideoWriter('output_yolomot.avi', fourcc, 40, (1280, 1024))
VIDEO_RESOLUTION_X = int(cap.get(3))
VIDEO_RESOLUTION_Y = int(cap.get(4))

## trackeri başlat
mot_tracker = Sort(max_age=5, min_hits=3, iou_threshold=0.3)

img = np.zeros([1024, 1280, 3], dtype=np.uint8)
img.fill(255)

local_tracks = []


def dist(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


def get_VCL():
    VCL_each_track_list = np.zeros(len(local_tracks))
    for index, track in enumerate(local_tracks):
        if len(track[1]) < 11:
            VCL_each_track_list[index] = -1
            continue
        counter = 0
        prev_coordinate = None
        for coordinate in track[1]:
            if prev_coordinate is None:
                prev_coordinate = coordinate
                continue
            else:
                d = dist(coordinate[0], coordinate[1], prev_coordinate[0], prev_coordinate[1])
                counter = counter + d
                prev_coordinate = coordinate

        #print(str(track[0]) + ":  " + str(counter) + " / " + str(len(track[1])))
        VCL_each_track_list[index] = round((counter * REAL_PIXEL_MEASUREMENT_IN_40X) / (len(track[1]) * FRAME_TIME), 3)
    VCL_each_track_list = VCL_each_track_list[VCL_each_track_list != -1]
    return VCL_each_track_list


def get_VSL():
    VSL_each_track_list = np.zeros(len(local_tracks))
    for index, track in enumerate(local_tracks):
        if len(track[1]) < 11:
            VSL_each_track_list[index] = -1
            continue
        counter = dist(track[1][0][0], track[1][0][1], track[1][-1][0], track[1][-1][1])
        VSL_each_track_list[index] = round((counter * REAL_PIXEL_MEASUREMENT_IN_40X) / (len(track[1]) * FRAME_TIME), 3)

        #print(str(track[0]) + ":  " + str(counter) + " / " + str(len(track[1])))

    VSL_each_track_list = VSL_each_track_list[VSL_each_track_list != -1]

    return VSL_each_track_list


def draw_sperma_with_index(index):
    track = local_tracks[index][1]
    if len(track) < 11:
        return
    plt.axis([0, VIDEO_RESOLUTION_X, 0, VIDEO_RESOLUTION_Y])
    # plt.ylim(0, VIDEO_RESOLUTION_Y)

    X = []
    Y = []
    for coordinate in track:
        X.append(coordinate[0])
        Y.append(coordinate[1])

    plt.plot(X, Y, '-o')
    plt.show()


## weight ve congfigure dosya yolları
weights_path = os.path.normpath(os.getcwd() + os.sep + "weights_cfg_file") + os.sep + 'head.weights'
cfg_path = os.path.normpath(os.getcwd() + os.sep + "weights_cfg_file") + os.sep + 'head.cfg'
box_color = (100, 90, 200)

## modeli opencv ile aç
model = cv2.dnn.readNetFromDarknet(cfg_path, weights_path)
model.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
model.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

layers = model.getLayerNames()
output_layer = [layers[layer[0] - 1] for layer in model.getUnconnectedOutLayers()]

count = 1
start_time = time.time()
while True:
    ret, frame = cap.read()
    if count == 161:
        break

    if not ret:
        break
    # print(count)
    count = count + 1

    resize_frame = cv2.resize(frame, None, fx=1, fy=1)
    # resize_frame = frame

    lab = cv2.cvtColor(resize_frame, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)
    # clahe = cv2.createCLAHE(clipLimit=1.5, tileGridSize=(5, 5))
    # l = clahe.apply(l)

    frame_width = resize_frame.shape[1]
    frame_height = resize_frame.shape[0]

    frame_blob = cv2.dnn.blobFromImage(l, 1 / 255, (160, 160), swapRB=True, crop=False)

    labels = ["sperma"]

    model.setInput(frame_blob)

    detection_layers = model.forward(output_layer)

    ############## NON-MAXIMUM SUPPRESSION - OPERATION 1 ###################

    ids_list = []
    boxes_list = []
    confidences_list = []

    ############################ END OF OPERATION 1 ########################

    for detection_layer in detection_layers:
        for object_detection in detection_layer:

            scores = object_detection[5:]
            predicted_id = np.argmax(scores)
            confidence = scores[predicted_id]

            if confidence > 0.40:
                label = labels[predicted_id]
                bounding_box = object_detection[0:4] * np.array([frame_width, frame_height, frame_width, frame_height])
                (box_center_x, box_center_y, box_width, box_height) = bounding_box.astype("int")

                start_x = int(box_center_x - (box_width / 2))
                start_y = int(box_center_y - (box_height / 2))

                ############## NON-MAXIMUM SUPPRESSION - OPERATION 2 ###################

                ids_list.append(predicted_id)
                confidences_list.append(float(confidence))
                boxes_list.append([start_x, start_y, int(box_width), int(box_height)])

                ############################ END OF OPERATION 2 ########################

    ############## NON-MAXIMUM SUPPRESSION - OPERATION 3 ###################

    max_ids = cv2.dnn.NMSBoxes(boxes_list, confidences_list, 0.5, 0.3)

    list = []
    now_sperma_list = []
    for max_id in max_ids:
        max_class_id = max_id[0]
        box = boxes_list[max_class_id]

        start_x = box[0]
        start_y = box[1]
        box_width = box[2]
        box_height = box[3]

        predicted_id = ids_list[max_class_id]
        label = labels[predicted_id]
        confidence = confidences_list[max_class_id]

        ############################ END OF OPERATION 3 ########################

        end_x = start_x + box_width
        end_y = start_y + box_height

        list.append([start_x, start_y, end_x, end_y])

        center_x_int = int((start_x + end_x) / 2)
        center_y_int = int((start_y + end_y) / 2)
        # now_sperma_list.append([center_x_int, center_y_int])

        label = "{}: {:.2f}%".format(label, confidence * 100)
        # print("predicted object {}".format(label))

        # cv2.rectangle(resize_frame, (start_x, start_y), (end_x, end_y), box_color, 2)
        # cv2.putText(resize_frame, label, (start_x, start_y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, box_color, 2)

    # update_list(now_sperma_list)

    track_bbs_ids = mot_tracker.update(np.asarray(list))
    for track in reversed(track_bbs_ids):
        if local_tracks[int(track[4]) - 1:int(track[4])]:
            pr_x = local_tracks[int(track[4]) - 1][1][-1][0]
            pr_y = local_tracks[int(track[4]) - 1][1][-1][1]

            now_x = (track[0] + track[2]) / 2
            now_y = (track[1] + track[3]) / 2
            d = dist(pr_x, pr_y, now_x, now_y)
            if d > 300:
                continue

            local_tracks[int(track[4]) - 1][1].append((now_x, now_y))
            ##print(local_tracks[int(track[4]) - 1:int(track[4])])
        else:
            local_tracks.append([int(track[4]) - 1, [((track[0] + track[2]) / 2, (track[1] + track[3]) / 2)]])
        # print(len(track_bbs_ids))
        '''random.seed(track[4])

        b = random.randint(0, 200)
        g = random.randint(0, 200)
        r = random.randint(0, 200)
        cv2.rectangle(resize_frame, (int(track[0]), int(track[1])), (int(track[2]), int(track[3])), (b, g, r), 2)
        cv2.putText(resize_frame, str(track[4]), (int(track[0]), int(track[1]) - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                    (0, 0, 0), 2)
        cv2.circle(img, (int((track[0] + track[2]) / 2), int((track[1] + track[3]) / 2)), radius=0, color=(b, g, r),
                   thickness=5)'''

        # if not tracks_list[track[4]]:
    '''cv2.imshow("video", resize_frame)
    cv2.imshow("img", img)
    out.write(resize_frame)'''

    if cv2.waitKey(1) == 27:
        break

end_time = time.time()
total_processing_time = end_time - start_time
print("Time taken: {}".format(total_processing_time))

VCL = get_VCL()
ss = ""
for v in VCL:
    ss = ss + " || " + str(v)
print(ss)
print("ortalama: " + str(np.mean(VCL[VCL >= 10])))

VSL = get_VSL()
s = ""
for v in VSL:
    s = s + " || " + str(v)
print(s)
print("ortalama: " + str(np.mean(VSL[VSL >= 10])))

'''for i in range(150):
    draw_sperma_with_index(i)'''

print("end") 

cv2.destroyAllWindows()
out.release()
