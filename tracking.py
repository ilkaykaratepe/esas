import os
import cv2
import numpy as np
import time
import math

import random

weights_path = os.path.normpath(os.getcwd() + os.sep + "weights_cfg_file") + os.sep + 'head.weights'
cfg_path = os.path.normpath(os.getcwd() + os.sep + "weights_cfg_file") + os.sep + 'head.cfg'
box_color = (100, 90, 200)

model = cv2.dnn.readNetFromDarknet(cfg_path, weights_path)
model.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
model.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

layers = model.getLayerNames()
output_layer = [layers[layer[0] - 1] for layer in model.getUnconnectedOutLayers()]

video_path = os.path.normpath(os.getcwd() + os.sep + "videos") + os.sep + 'ESAS TİP 40X Kayıt 4.mp4'
cap = cv2.VideoCapture(video_path)

img = np.zeros([1024, 1280, 3], dtype=np.uint8)
img.fill(255)

tracks_list = []
tracks_state = []


def dist(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


fourcc = cv2.VideoWriter_fourcc(*'MPEG')
out = cv2.VideoWriter('output_yolo.avi', fourcc, 40, (1280, 1024))
frame_number = 1
start_time = time.time()
while True:
    ret, frame = cap.read()
    if not ret:
        break

    # resize_frame = cv2.resize(frame, None, fx=1, fy=1)
    resize_frame = frame

    lab = cv2.cvtColor(resize_frame, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)

    frame_width = resize_frame.shape[1]
    frame_height = resize_frame.shape[0]

    frame_blob = cv2.dnn.blobFromImage(l, 1 / 255, (160, 160), swapRB=True, crop=False)

    labels = ["sperma"]

    model.setInput(frame_blob)

    detection_layers = model.forward(output_layer)

    ############## NON-MAXIMUM SUPPRESSION - OPERATION 1 ###################

    ids_list = []
    boxes_list = []
    confidences_list = []

    ############################ END OF OPERATION 1 ########################

    for detection_layer in detection_layers:
        for object_detection in detection_layer:

            scores = object_detection[5:]
            predicted_id = np.argmax(scores)
            confidence = scores[predicted_id]

            if confidence > 0.40:
                label = labels[predicted_id]
                bounding_box = object_detection[0:4] * np.array([frame_width, frame_height, frame_width, frame_height])
                (box_center_x, box_center_y, box_width, box_height) = bounding_box.astype("int")

                start_x = int(box_center_x - (box_width / 2))
                start_y = int(box_center_y - (box_height / 2))

                ############## NON-MAXIMUM SUPPRESSION - OPERATION 2 ###################

                ids_list.append(predicted_id)
                confidences_list.append(float(confidence))
                boxes_list.append([start_x, start_y, int(box_width), int(box_height)])

                ############################ END OF OPERATION 2 ########################

    ############## NON-MAXIMUM SUPPRESSION - OPERATION 3 ###################

    max_ids = cv2.dnn.NMSBoxes(boxes_list, confidences_list, 0.5, 0.3)

    for max_id in max_ids:
        max_class_id = max_id[0]
        box = boxes_list[max_class_id]

        start_x = box[0]
        start_y = box[1]
        box_width = box[2]
        box_height = box[3]

        predicted_id = ids_list[max_class_id]
        label = labels[predicted_id]
        confidence = confidences_list[max_class_id]

        ############################ END OF OPERATION 3 ########################

        end_x = start_x + box_width
        end_y = start_y + box_height

        center_x_int = int((start_x + end_x) / 2)
        center_y_int = int((start_y + end_y) / 2)

        if frame_number == 1:
            tracks_list.append([(center_x_int, center_y_int)])
            tracks_state.append([True, 7])
            continue

        else:
            best_match = ()
            d_range = 150
            index_range = -1
            for index, i in enumerate(tracks_list):
                if tracks_state[index][1] == 0:
                    continue
                stored_x = i[len(i) - 1][0]
                stored_y = i[len(i) - 1][1]

                d = dist(stored_x, stored_y, center_x_int, center_y_int)
                if d < d_range:
                    d_range = d
                    index_range = index
                    tracks_state[index][0] = True
                else:
                    tracks_state[index][0] = False

            if index_range == -1:
                tracks_list.append([(center_x_int, center_y_int)])
                tracks_state.append([True, 7])

            else:
                tracks_list[index_range].append((center_x_int, center_y_int))
                for i_st, st in enumerate(tracks_state):
                    if not st[0]:
                        tracks_state[i_st][1] = tracks_state[i_st][1] - 1

        for seed, track in enumerate(tracks_list):

            random.seed(seed)
            b = random.randint(0, 190)
            g = random.randint(0, 190)
            r = random.randint(0, 190)

            for i in reversed(track):
                print(i)
                cv2.circle(img, i, radius=0, color=(b, g, r), thickness=10)
                #cv2.putText(img, str(seed), i, cv2.FONT_HERSHEY_SIMPLEX, 0.6, (b, g, r), 2)

        label = "{}: {:.2f}%".format(label, confidence * 100)
        # print("predicted object {}".format(label))

        cv2.rectangle(resize_frame, (start_x, start_y), (end_x, end_y), box_color, 2)
        cv2.circle(resize_frame, (center_x_int, center_y_int), radius=0, color=(90, 50, 200), thickness=7)
        cv2.putText(resize_frame, label, (start_x, start_y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, box_color, 2)

    cv2.imshow("img", img)
    cv2.imshow("video", resize_frame)
    out.write(resize_frame)

    frame_number = frame_number + 1
    if frame_number == 900:
        break
    if cv2.waitKey(1) == 27:
        break

end_time = time.time()
total_processing_time = end_time - start_time
print("Time taken: {}".format(total_processing_time))

for track_id, track in enumerate(tracks_list):
    img.fill(200)
    random.seed(track_id)
    b = random.randint(0, 255)
    g = random.randint(0, 255)
    r = random.randint(0, 255)
    for i in track:
        cv2.circle(img, i, radius=0, color=(b, g, r), thickness=5)
    cv2.imwrite("filename " + str(track_id) + ".jpg", img)

cv2.destroyAllWindows()
out.release()
